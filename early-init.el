(setq package-user-dir
      (let ((elpa-dir-name (format "elpa_%s" emacs-major-version)))
					;default = ~/.emacs.d/elpa/
	(file-name-as-directory
	 (expand-file-name
	  elpa-dir-name
	  user-emacs-directory))))

