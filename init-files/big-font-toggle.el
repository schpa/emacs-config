(defun adjust-font-to-scale (arg)
  (interactive "p")
  (text-scale-adjust arg))

(provide 'big-font-toggle)
