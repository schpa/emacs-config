(require 'big-font-toggle)
(require 'window-functions)

(defun defk (k f)
  "shortcut for deffing k to f"
  (global-set-key (kbd k) f))

(setq factor 1.5)

(defk "M-o" (lambda() (interactive) (projectile-find-file)))

(defk "M-z" 'undo)
(defk "M-c" 'kill-ring-save)

;; I like the original definition better
;(defk "C-l" 'paredit-recentre-on-sexp)
(defk "C-z" 'repeat)
;; (defk "C-x C-r" 'recentf-open-files)
(defk "M-C-f" 'toggle-frame-fullscreen)
(defk "M-s-<left>" 'previous-buffer)
(defk "M-s-<right>" 'next-buffer)

;; window manipulation
(defk "C-\\" 'window-swap-states)
(defk "C-|" 'transpose-frame)

(defk "C-x C-m" 'magit-status)
(defk "M-e" 'recentf-open-files)
(defk "C-<return>" 'cider-eval-last-sexp)
(defk "M-<return>" 'cider-eval-defun-at-point)
;;(defk "M-C-<return>" 'cider-)

(defk "M-f" 'forward-sexp)
(defk "M-b" 'backward-sexp)

;; multiple cursors

(defk "C-." 'mc/mark-next-like-this)
(defk "C->" 'mc/mark-previous-like-this)
(defk "C-<" 'mc/mark-all-like-this)
(defk "C-]" 'parinfer-toggle-mode)

;; expand region
(defk "M-w" 'er/expand-region)
(defk "M-W" 'kill-ring-save)
(defk "M-l" 'cider-find-and-clear-repl-output)

(defk "M-s" 'save-buffer)
(defk "M-<return>" 'cider-eval-defun-at-point)
(defk "C-M-<return>" 'cider-eval-defun-to-comment)
(defk "M-w" 'er/expand-region)

;; files
(global-unset-key (kbd "M-s"))
(defk "M-s" 'save-buffer)

;; often observed keybinding that I IMO think is wrong in lisp-setups:
(defk "M-<left>" (lambda () (interactive) (forward-symbol -1)))
(defk "M-<right>" 'forward-symbol)

;; projectile
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
(defk "C-o" 'other-window)
(defk "M-s-<enter>" 'cider-eval-defun-to-comment)
(defk "M-s-l" 'cider-format-buffer)

;; intellij stuff
(defk "M-S-<backspace>" (lambda () (interactive) (goto-last-change)))

(defk "M-e" 'ido-recentf-open)

(defk "<f4>" 'lispy-mode)
(defk "<f5>" 'linum-mode)
(defk "<f6>" 'toggle-truncate-lines)
(defk "<f7>" (lambda () (interactive) () (eshell)))
(defk "<f8>" 'dired)
(defk "<f9>" 'ibuffer)

(defk "<f15>" (lambda () (interactive) (adjust-font-to-scale (- factor))))
(defk "S-<f15>" (lambda () (interactive) (adjust-font-to-scale factor)))
(defk "M-<f15>" (lambda () (interactive) (next-theme 1)))

(defk "S-M-<f15>" (lambda () (interactive) (next-theme -1)))

(require 'lispy)
(define-key lispy-mode-map (kbd "M-<return>") 'cider-eval-last-sexp)
(define-key lispy-mode-map (kbd "C-<return>") 'cider-eval-defun-to-comment )

;;  hs-minor-modep
(defk "M-[" 'hs-hide-block)
(defk "M-]" 'hs-show-block)
(defk "M-s-[" 'hs-hide-all)
(defk "M-s-]" 'hs-show-all)

;;
;;
;;
;;

(provide 'chris-shortcuts)
