(defun rotate-windows (arg)
  "Rotate your windows; use the prefix argument to rotate the other direction"
  (interactive "P")
  (if (not (> (count-windows) 1))
      (message "You can't rotate a single window!")
    (let* ((rotate-times (prefix-numeric-value arg))
           (direction (if (or (< rotate-times 0) (equal arg '(4)))
                          'reverse 'identity)))
      (dotimes (_ (abs rotate-times))
        (dotimes (i (- (count-windows) 1))
          (let* ((w1 (elt (funcall direction (window-list)) i))
                 (w2 (elt (funcall direction (window-list)) (+ i 1)))
                 (b1 (window-buffer w1))
                 (b2 (window-buffer w2))
                 (s1 (window-start w1))
                 (s2 (window-start w2))
                 (p1 (window-point w1))
                 (p2 (window-point w2)))
            (set-window-buffer-start-and-point w1 b2 s2 p2)
            (set-window-buffer-start-and-point w2 b1 s1 p1)))))))

(defun window-split-toggle ()
  "Toggle between horizontal and vertical split with two windows."
  (interactive)
  (if (> (length (window-list)) 2)
      (error "Can't toggle with more than 2 windows!")
    (let ((func (if (window-full-height-p)
                    #'split-window-vertically
                  #'split-window-horizontally)))
      (delete-other-windows)
      (funcall func)
      (save-selected-window
        (other-window 1)
        (switch-to-buffer (other-buffer))))))

;; theme selection
(load "~/.emacs.d/alabaster-theme.el")
;(require 'alabaster)

(setq active-theme-idx 2)
(setq themes (custom-available-themes))

(defun next-theme (arg)  
  (interactive)
  (let* ((idx active-theme-idx)
	 (total-themes (length themes))
         (next (cond 
	       ((= arg 1) (if (< idx total-themes) (+ 1 idx) 0))
	       ((= arg -1) (if (< idx 1 )
			       (-total-themes 1)
			     (- idx 1)))
	       (t 0))))
    (progn
      (setq active-theme-idx next)
      (message (format "Changed theme to %s (%d)"
		       (nth active-theme-idx themes) active-theme-idx))
      (load-theme (nth active-theme-idx themes))
      (blink-cursor-mode t)
      (load-theme 'alabaster)
      (set-cursor-color "#ff3300"))))

(next-theme 0)

(provide 'window-functions)
  
