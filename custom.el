(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(cider-default-repl-command "clj")
 '(cider-jack-in-default "clj")
 '(custom-safe-themes
   (quote
    ("f6f094ad12fc2a9dd644814959ba39af636c4149cd7d3182fc2011f9c4e034fe" "b0bed61b3d8a62c67a9b9641258689320cfb7ed32fd6fe61de796d898a21ad68" "f9a6163e48aa54df0380687d171d9481961ab6a501266982dc354b6b20d1f8a4" default)))
 '(delete-selection-mode t)
 '(fringe-mode (quote (nil . 0)) nil (fringe))
 '(haskell-process-auto-import-loaded-modules t)
 '(haskell-process-log t)
 '(haskell-process-suggest-remove-import-lines t)
 '(haskell-process-type (quote cabal-repl))
 '(inhibit-startup-echo-area-message t)
 '(line-number-mode nil)
 '(minimap-mode nil)
 '(nxml-child-indent 5)
 '(package-selected-packages
   (quote
    (hungry-delete vimish-fold diff-hl dired-x keyfreq delight spaceline recently swift-mode ido-vertical-mode lispy ag transpose-frame ibuffer-vc magit which-key ace-window multiple-cursors clojure-mode dash exec-path-from-shell sesman use-package dired-narrow expand-region projectile ## cider-hydra package+ cider-eval-sexp-fu adjust-parens cider company wsd-mode window-numbering smex rainbow-mode rainbow-delimiters powerline paren-face paredit markdown-mode json-reformat haskell-mode go-eldoc ghc fuzzy flycheck auto-complete ace-jump-mode)))
 '(polling-period 2)
 '(send-mail-function (quote mailclient-send-it))
 '(show-paren-mode t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(linum ((t (:inherit (shadow default) :background "#2b2a24"))))
 '(sml-font-lock-symbols-keywords ((t (:foreground "red")))))
