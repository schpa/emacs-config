(load "~/.emacs.d/init-files/cps-utils.el")
(tool-bar-mode 0)
(set-face-attribute 'default nil :height 180)
(desktop-read)


(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
		    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  (when no-ssl
    (warn "Your version of Emacs does not support SSL connections."))

  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    (add-to-list 'package-archives (cons "gnu" (concat proto "://elpa.gnu.org/packages/")))))

(package-initialize)

;;
;;
;;
;; 


(add-to-list 'load-path "~/.emacs.d/melpa")
(add-to-list 'load-path "~/.emacs.d/init-files")

;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(setq custom-file "~/.emacs.d/custom.el")

(load custom-file)
(eval-when-compile
  (require 'use-package))

(use-package ido-vertical-mode
  :ensure t
  :init
  (ido-vertical-mode 1)
  :config
  (setq ido-vertical-define-keys 'C-n-C-p-up-down-left-right)
  (setq ido-enable-flex-matching t)
  (setq ido-create-new-buffer 'always)
  (setq ido-everywhere t))

(use-package projectile :ensure t)
(use-package lispy :ensure t)
(use-package paredit :ensure t)
(use-package company :ensure t)
(use-package expand-region :ensure t)
(use-package which-key :ensure t)
(use-package cider :ensure t)
(use-package multiple-cursors :ensure t)
(use-package magit :ensure t)

(use-package smex
  :ensure t
  :init (smex-initialize)
  :bind ("M-x" . smex))

(setq dired-omit-mode t) 

(require 'dired-x)
(setq dired-omit-files (concat dired-omit-files "\\|^\\..+$"))
(setq-default dired-omit-files-p t)

(defun ido-recentf-open ()
  "Use `ido-completing-read' to find a recent file."
  (interactive)
  (if (find-file (ido-completing-read "Find recent file: " recentf-list))
      (message "Opening file...")
    (message "Aborting")))

;;
;;
;;
;; 

(require 'window-functions)
;(require 'multiple-cursors)
;(require 'expand-region)
;(require 'ace-window)
;(require 'which-key)
(require 'fira-code-mode)
(require 'goto-last-change)
 
(setq mac-command-modifier 'meta
      mac-right-option-modifier 'super
      mac-option-modifier 'super)

(setq xref-prompt-for-identifier nil)

;;* Customize

(defmacro csetq (variable value)
  `(funcall (or (get ',variable 'custom-set) 'set-default) ',variable ,value))
(setq ibuffer-saved-filter-groups
      '(("home"
         ("emacs-config" (or (filename . "init.el")))
         ("code" (filename . "\*.cljs")))))

(require 'eldoc)

(eldoc-add-command
 'paredit-backward-delete
 'paredit-close-round)

(defun MY-HOOK-FUNC ()
  (message "MY-HOOK-FUNC"))

;;
;;
;;
;; backup
		
(setq backup-directory-alist `(("." . "~/.saves")))

;; eshell picking up env variables and aliases from ...

(setq shell-file-name "zsh")

(setq shell-command-switch "-ic")

(setq org-src-tab-acts-natively t)

(require 'ob-clojure)
(setq org-babel-clojure-backend 'cider)
;(require 'cider)
(setq cider-known-endpoints '(("projects/org" "127.0.0.1" "54945")
			      ("projects/herweb" "127.0.0.1" "0")))

(use-package spaceline
  :ensure t
  :config
  (setq-default mode-line-format '("%e" (:eval (spaceline-ml-main)))))

(use-package spaceline-config :ensure spaceline
  :config
  (spaceline-emacs-theme))

(use-package spaceline-config
  :ensure spaceline
  :config
  (spaceline-install
   'main
   '((buffer-modified)
     ((remote-host buffer-id) :face highlight-face)
     (process :when active))
   '((selection-info :face 'region :when mark-active)
     ((flycheck-error flycheck-warning flycheck-info) :when active)
     (which-function)
     (version-control :when active)
     (line-column)
     (global :when active)
     (major-mode))))

(setq-default
 powerline-height 24
 powerline-default-separator 'arrow
 spaceline-flycheck-bullet "❖ %s"
 spaceline-separator-dir-left '(right . right)
 spaceline-separator-dir-right '(left . left))

(defun run-with-lisp-hooks ()
  (interactive)
  (progn
    (lispy-mode 1)
    (fira-code-mode)
    (paredit-mode 0)
    (hs-minor-mode 1)
    (toggle-truncate-lines 1)))

(use-package keyfreq
  :ensure t
  :config
  (keyfreq-mode 1)
  (keyfreq-autosave-mode 1))

(use-package hungry-delete
  :ensure t
  :config
  (global-hungry-delete-mode))

;; (keyfreq-show)

;;
;;
;;
;;
(set-cursor-color "#ff0000")
(server-start)

(savehist-mode 1)
(put 'narrow-to-region 'disabled nil)
(global-company-mode 1)
(menu-bar-mode nil)
(projectile-mode +1)
(next-theme 0)
;(set-scroll-bar-mode nil)
(company-mode 1) 
(tool-bar-mode 0)
(hl-line-mode 0)
(recentf-mode 1)
(setenv "PATH" (concat (getenv "PATH") ":/usr/local/bin"))
(defalias 'yes-or-no-p 'y-or-n-p)

(setq make-backup-files nil
      auto-save-default nil 
      ibuffer-expert t
      ibuffer-show-empty-filter-groups nil
      cider-repl-display-help-banner nil
      ring-bell-function 'ignore
      recentf-max-menu-items 10
      initial-scratch-message nil
      inhibit-startup-message t 
      default-directory (concat (getenv "HOME") "/projects/temp/exp")
      exec-path (append exec-path '("/usr/local/bin"))
      parinfer-lighters '(" PInd" . " Pnfr"))

(which-key-mode 1)

;;
;;
;;
;; 

(add-hook
 'emacs-lisp-mode-hook
 'run-with-lisp-hooks)

(add-hook 'clojure-mode-hook 'run-with-lisp-hooks)

(add-hook 'clojure-mode-hook
          (lambda () (add-hook 'before-save-hook #'MY-HOOK-FUNC nil 'local)))

(add-hook 'before-save-hook #'cider-format-buffer)

(add-hook 'ibuffer-mode-hook
	  '(lambda ()
	     (ibuffer-auto-mode 1)
	     (ibuffer-switch-to-saved-filter-groups "code")))

(add-hook 'ibuffer-hook
	  (lambda ()
	    (ibuffer-vc-set-filter-groups-by-vc-root)
	    (unless (eq ibuffer-sorting-mode 'alphabetic)
	      (ibuffer-do-sort-by-alphabetic))))

(require 'chris-shortcuts)
